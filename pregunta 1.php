  
<?php
	$matriz = [[1,2],[2,4,3]] ;

	# (Dimension) devuelve el número entero que define la dimensión del arreglo o matriz en su mayor profundidad.
	echo 'Dimension: '.dimension($matriz).'<br>';
	# (Uniformidad) Devuelve true o false según el siguiente criterio: -True: Si el arreglo o matriz contiene la misma cantidad de elementos en cada una de sus dimensiones (Matriz uniforme). -False: Caso contrario.
	echo 'Uniformidad: '.straight($matriz).'<br>';
	#-> (Suma) devuelve el número entero resultado de la suma de todos los números incluídos en la matriz sin importar el tamaño o dimensión.
	echo 'Suma: '.compute($matriz).'<br>';
	
	#Se crea una funcion que de acuerdo a la dimensiom del arreglo, realiza el recorrido
	function dimension($matriz){
		return count($matriz);
	}

	#Se crea una funcion que compara el primer vector com el ultimo.
	function straight($matriz){
		$elemento=[];
		for ($i=0; $i<count($matriz); $i++) {
			if (count($matriz[1]) == count($matriz[$i])) {
				return 'true';
			}else{
				return 'false';
			}
		}

	}
	#Se crea una funcion que suma cada uno de los valores que componen la matriz.
	function compute($matriz){
		$suma=0;
		foreach ($matriz as $a => $b) {
			if (dimension($matriz) > 1) {
				foreach ($b as $c => $d) {
					if (dimension($matriz) > 2) {
						foreach ($d as $e => $f) {
							$suma+=$f;
						}
					}else{
						$suma+=$d;
					}
				}
			}else{
				$suma+=$b;
			}
		}
		return $suma;
	}
?>
